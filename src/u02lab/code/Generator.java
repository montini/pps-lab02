package u02lab.code;

import sun.nio.cs.Surrogate;
import u02lab.code.logic.GeneratorLogic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Generator implements SequenceGenerator {
    private List<Integer> numbers;
    private int pointer;

    public Generator(GeneratorLogic<List<Integer>> generatorLogic){
        this.numbers = generatorLogic.generate();
        this.pointer = 0;
    }

    @Override
    public Optional<Integer> next() {
        Optional<Integer> next = Optional.empty();
        if (this.pointer < this.numbers.size()) {
            next = Optional.of(this.numbers.get(this.pointer));
        }
        this.pointer++;
        return next;
    }

    @Override
    public void reset() {
        this.pointer = 0;
    }

    @Override
    public boolean isOver() {
        return !(this.pointer < this.numbers.size());
    }

    @Override
    public List<Integer> allRemaining() {
        return this.numbers.stream().skip(this.pointer).collect(Collectors.toList());
    }
}
