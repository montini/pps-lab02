package u02lab.code.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Random implements GeneratorLogic<List<Integer>> {

    int number;

    public Random(int number) {
        this.number = number;
    }

    @Override
    public List<Integer> generate() {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i< number; i++) {
            numbers.add((int) Math.round(Math.random()));
        }
        return numbers;
    }
}
