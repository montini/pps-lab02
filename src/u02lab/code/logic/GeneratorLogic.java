package u02lab.code.logic;

public interface GeneratorLogic<T> {

    T generate();
}
