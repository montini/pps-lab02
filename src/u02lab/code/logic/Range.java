package u02lab.code.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Range implements GeneratorLogic<List<Integer>> {

    int start;
    int stop;
    public Range(int start, int stop) {
        this.start = start;
        this.stop = stop;
    }

    @Override
    public List<Integer> generate() {
        List<Integer> numbers = new ArrayList<>();
        for (int number = this.start; number<=this.stop; number++) {
            numbers.add(number);
        }
        return numbers;
    }
}
