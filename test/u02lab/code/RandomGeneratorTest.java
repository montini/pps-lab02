package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import u02lab.code.logic.Random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

public class RandomGeneratorTest {

    private static int NUMBERS = 8;
    private SequenceGenerator generator;

    @Before
    public void setup() {
        //this.generator = new RandomGenerator(NUMBERS);
        this.generator = new Generator(new Random(NUMBERS));
    }

    @Test
    public void testOverflow() {
        for (int i = 0; i < NUMBERS; i++) {
            Assert.assertTrue(this.generator.next().isPresent());
        }
        Assert.assertFalse(this.generator.next().isPresent());
    }

    @Test
    public void testBinaryValues() {
        for (int i = 0; i < NUMBERS; i++) {
            int bit = generator.next().get();
            Assert.assertThat(bit,  anyOf(is(0),is(1)));
        }
    }

    @Test
    public void testReset() {
        List<Integer> savedNumbers = new ArrayList<>();
        for (int i = 0; i < NUMBERS-2; i++) {
            savedNumbers.add(this.generator.next().get());
        }
        this.generator.reset();
        for (int i = 0; i < NUMBERS-2; i++) {
            Assert.assertEquals(savedNumbers.get(i),this.generator.next().get());
        }
        for (int i = NUMBERS-2; i < NUMBERS; i++) {
            Assert.assertTrue(this.generator.next().isPresent());
        }
        Assert.assertEquals(Optional.empty(), this.generator.next());
    }

    @Test
    public void testIsOver() {
        this.generator.next();
        Assert.assertFalse(this.generator.isOver());
        nextUntilEnd();
        Assert.assertTrue(this.generator.isOver());
    }

    @Test
    public void testAllRemaining() {
        this.generator.next();
        this.generator.next();
        Assert.assertEquals(NUMBERS-2, this.generator.allRemaining().size());
    }

    private void nextUntilEnd() {
        for (int i = 0; i < NUMBERS; i++) {
            this.generator.next();
        }
    }
}