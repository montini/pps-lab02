package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import u02lab.code.logic.Range;

import java.util.Arrays;

import static org.junit.Assert.*;

public class RangeGeneratorTest {

    private static int START = 3;
    private static int STOP = 5;
    private SequenceGenerator generator;

    @Before
    public void setup() {
        //this.generator = new RangeGenerator(START,STOP);
        this.generator = new Generator(new Range(START, STOP));
    }

    @Test
    public void testFromStartToEnd() {
        Assert.assertEquals(new Integer(3), this.generator.next().get());
        Assert.assertEquals(new Integer(4), this.generator.next().get());
        Assert.assertEquals(new Integer(5), this.generator.next().get());
    }

    @Test
    public void testOverflow() {
        this.generator.next();
        this.generator.next();
        this.generator.next();

        Assert.assertFalse(this.generator.next().isPresent());
    }

    @Test
    public void testReset() {
        this.generator.next();
        this.generator.next();
        this.generator.reset();
        Assert.assertEquals(new Integer(3), this.generator.next().get());
    }

    @Test
    public void testIsOver() {
        this.generator.next();
        Assert.assertFalse(this.generator.isOver());
        this.generator.next();
        this.generator.next();
        Assert.assertTrue(this.generator.isOver());
    }

    @Test
    public void testAllRemaining() {
        this.generator.next();
        Assert.assertEquals(Arrays.asList(4,5), this.generator.allRemaining());
    }
}